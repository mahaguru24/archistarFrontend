import {shallowMount} from '@vue/test-utils'
import ShowSelectedState from '@/components/ShowSelectedState.vue'
import {capitalize} from '../../src/lib/helpers'

// register filter before running tests
import '@/filters'

describe('ShowSelectedState.vue', () => {
    it('renders props.selected when passed', () => {
        const selected = 'all'
        const passValue = capitalize(selected)
        const wrapper = shallowMount(ShowSelectedState, {
            propsData: { selected }
        })
        expect(wrapper.text()).toMatch(passValue)
    })
})
