import Vue from 'vue'
import {capitalize} from '../lib/helpers'
// write all filters here

Vue.filter('capitalize', capitalize)
